<?php
define("API_POOL_SIZE", "pool_size.php");

function media_stats_formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
         $bytes /= pow(1024, $pow);
        //$bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
}

function media_stats_get_pool_size_url($pools, $api_url) {
        $pools_param = '';
        $counter = 0;
        $pools_parsed = explode(',', $pools);
        foreach ($pools_parsed as $pool) {
                                        $pools_param .= $pool;
                                        $counter = $counter + 1;

                                        if ($counter < count($pools_parsed)) {
                                                        $pools_param .= ',';
                                        }
        }

        return $api_url . '/' . API_POOL_SIZE . '?pools=' . $pools_param;
}

function media_stats_display_table($data) {
        $table = "";

        $table .=  "<table id='media_pools'>";
        $table .=  "<thead>";
        $table .=  "<tr>";
        $table .=  "<th>Pool</th>";
        $table .=  "<th>Total Space</th>";
        $table .=  "<th>Used Space</th>";
        $table .=  "<th>Free Space</th>";
        $table .=  "</tr>";
        $table .=  "</thead>";
        $table .=  "<tbody>";
        foreach($data->{'pools'} as $pool) {
                        $pool_name_tmp = explode('/',$pool->{'pool'});
                        $pool_name_tmp = $pool_name_tmp[count($pool_name_tmp) - 1];

                        switch ($pool_name_tmp) {
                                        case 'tvshows':
                                                        $pool_name = 'TV Shows';
                                                        break;
                                        case 'movies':
                                                        $pool_name = 'Movies';
                                                        break;
                                        default:
                                                        $pool_name = ucfirst($pool_name_tmp);
                        }

                        $table .=  "<tr>";
                                        $table .=  "<td>" . $pool_name . "</td>";
                                        $table .=  "<td>" . media_stats_formatBytes(floatval($pool->{'total_space'})) . "</td>";
                                        $table .=  "<td>" . media_stats_formatBytes(floatval($pool->{'used_space'})) . "</td>";
                                        $table .=  "<td>" . media_stats_formatBytes(floatval($pool->{'free_space'})) . "</td>";
                        $table .=  "</tr>";
        }
        $table .=  "</thead>";
        $table .=  "</table>";

        return $table;
}

function media_stats_get_data() {
        $pools = variable_get('mediastats_pools', t("/path/to/media/pool_a,/path/to/media/pool_b"));

        if (variable_get('mediastats_cache_results', false)) {
                        watchdog('mediastats', 'Checking cache', NULL, WATCHDOG_DEBUG);
                        watchdog('mediastats', 'Cache contents: ' . variable_get('mediastats_cache_results_json', NULL), NULL, WATCHDOG_DEBUG);

                        $json = variable_get('mediastats_cache_results_json');

                        watchdog('mediastats', 'JSON: ' . $json, NULL, WATCHDOG_DEBUG);

                        $data = json_decode($json, true);
                        $cache_time = $data['server_time'];
                        $curtime = time();
                        watchdog('mediastats', 'JSON cached time: ' . $cache_time, NULL, WATCHDOG_DEBUG);

                        watchdog('mediastats', 'Current Time: ' . $curtime, NULL, WATCHDOG_DEBUG);
                        watchdog('mediastats', 'Cache Time: ' . $cache_time, NULL, WATCHDOG_DEBUG);

                        if($cache_time == NULL || ($curtime - $cache_time) > variable_get('mediastats_expire_cache_seconds', 60)) {
                                        watchdog('mediastats', 'Cache Expired', NULL, WATCHDOG_DEBUG);

                                        $json = file_get_contents(media_stats_get_pool_size_url($pools, variable_get('mediastats_api_url', t('http://localhost/api/v1'))));
                                        if ($json) {
                                                        watchdog('mediastats', 'Saving Cache', NULL, WATCHDOG_DEBUG);
                                                        variable_set('mediastats_cache_results_json', $json);
                                        }

                                        else {
                                                        $json = variable_get('mediastats_cache_results_json', NULL);

                                                        watchdog('mediastats', 'Media Server Unavailable, attempting to use cache.', NULL, WATCHDOG_ERROR);
                                        }
                        }

                        else {
                                        watchdog('mediastats', 'Cache Valid', NULL, WATCHDOG_DEBUG);
                        }
        }

        else {
                        watchdog('mediastats', 'Skipping cache', NULL, WATCHDOG_DEBUG);

                        $json = file_get_contents(get_pool_size_url($pools, variable_get('mediastats_api_url', t('http://localhost/api/v1'))));

                        if ($json) {
                                                        watchdog('mediastats', 'Saving Cache', NULL, WATCHDOG_DEBUG);
                                                        variable_set('mediastats_cache_results_json', $json);
                        }

                        else {
                                        $json = NULL;

                                        watchdog('mediastats', 'Media Server Unavailable.', NULL, WATCHDOG_ERROR);
                        }
        }

        watchdog('mediastats', 'Final JSON: ' . $json, NULL, WATCHDOG_DEBUG);

        if ($json == NULL) {
                        $dataArray = NULL;
        }

        else {
                        $dataArray = json_decode($json);
        }

        return $dataArray;
}

function media_stats_display_chart($pool, $width = '900px', $height = '500px') {
        $pool_name_tmp = explode('/',$pool->{'pool'});
        $pool_name_tmp = $pool_name_tmp[count($pool_name_tmp) - 1];

        drupal_add_js('
                google.load("visualization", "1", {packages:["corechart"]});
                  google.setOnLoadCallback(drawChart);
                  function drawChart() {

                        var data = google.visualization.arrayToDataTable([
                          ["Pool", "Space"],
                          ["Free Bytes",     ' . floatval($pool->{'free_space'}) . '],
                          ["Used Bytes",      ' . floatval($pool->{'used_space'}) . '],
                        ]);

                        var options = {
                          title: "' . ucfirst($pool_name_tmp) . ': Space Utilization"
                        };

                        var chart = new google.visualization.PieChart(document.getElementById("piechart_' . $pool_name_tmp . '"));

                        chart.draw(data, options);
                  }
        ', 'inline');

        return array(
                'html'  => '<div id="piechart_' . $pool_name_tmp . '" style="width: ' . $width . '; height: ' . $height . ';"></div>',
                'id'    => "piechart_" . $pool_name_tmp
        );
}
