<?php
        /**
         * @file
         * Module file for mediastats.
         */

        /**
         * @defgroup mediastats Media Stats: Block
         * @ingroup mediastats
         * @{
         * Demonstrates code creation of blocks.
         *
         * This is an example outlining how a module can define blocks that can be
         * displayed on various pages of a site, or how to alter blocks provided by
         * other modules.
         */

        include('stats.inc');
        drupal_add_js('https://www.google.com/jsapi');

        /**
         * Implements hook_menu().
         *
         * Provides a default page to explain what this module does.
         */
        function mediastats_menu() {
          $items['mediastats/stats_block'] = array(
                'page callback' => 'mediastats_page',
                'access callback' => TRUE,
                'title' => 'Media Stats',
          );
          return $items;
        }

        /**
         * Simple page function to explain what the block example is about.
         */
        function mediastats_page() {
                $page = array(
                        '#type' => 'markup',
                        '#markup' => t('The Block Example provides three sample blocks which demonstrate the various block APIs. To experiment with the blocks, enable and configure them on <a href="@url">the block admin page</a>.', array('@url' => url('admin/structure/block'))),
                );
                return $page;
        }

        /**
         * Implements hook_block_info().
         *
         * This hook declares what blocks are provided by the module.
         */
        function mediastats_block_info() {
                // This hook returns an array, each component of which is an array of block
                // information. The array keys are the 'delta' values used in other block
                // hooks.
                //
                // The required block information is a block description, which is shown
                // to the site administrator in the list of possible blocks. You can also
                // provide initial settings for block weight, status, etc.
                //
                // Many options are defined in hook_block_info():
                $blocks['stats'] = array(
                        // info: The name of the block.
                        'info' => t('Media Server Stats'),
                        'cache' => DRUPAL_NO_CACHE,
                );

                return $blocks;
        }


        /**
         * Implements hook_block_configure().
         *
         * This hook declares configuration options for blocks provided by this module.
         */
        function mediastats_block_configure($delta = '') {
                $form = array();

                // The $delta parameter tells us which block is being configured.
                // In this example, we'll allow the administrator to customize
                // the text of the 'configurable text string' block defined in this module.
                if ($delta == 'stats') {
                        // All we need to provide is the specific configuration options for our
                        // block. Drupal will take care of the standard block configuration options
                        // (block title, page visibility, etc.) and the save button.
                        $form['mediastats_api_url'] = array(
                                '#type' => 'textfield',
                                '#title' => t('API URL'),
                                '#size' => 60,
                                '#description' => t('The URL to the ServerStats API.'),
                                '#default_value' => variable_get('mediastats_api_url', t('http://localhost/api/v1')),
                        );

                        $form['mediastats_pools'] = array(
                                '#type' => 'textfield',
                                '#title' => t('Media Pools'),
                                '#size' => 60,
                                '#description' => t('A comma delimited list of absolute paths to the media pools.'),
                                '#default_value' => variable_get('mediastats_pools', t("/path/to/media/pool_a,/path/to/media/pool_b")),
                        );

                        $form['mediastats_cache_results'] = array(
                                '#type' => 'checkbox',
                                '#title' => t('Cache Results'),
                                '#size' => 60,
                                '#description' => t('Cache the results from the media server.'),
                                '#default_value' => variable_get('mediastats_cache_results', t("/path/to/media/pool_a,/path/to/media/pool_b")),
                        );

                        $form['mediastats_expire_cache_seconds'] = array(
                                '#type' => 'textfield',
                                '#title' => t('Expire Cache Results After'),
                                '#size' => 60,
                                '#description' => t('The number of seconds after which the cache should expire.'),
                                '#default_value' => variable_get('mediastats_expire_cache_seconds', 60),
                        );
                }

                return $form;
        }

        /**
         * Implements hook_block_save().
         *
         * This hook declares how the configured options for a block
         * provided by this module are saved.
         */
        function mediastats_block_save($delta = '', $edit = array()) {
                // We need to save settings from the configuration form.
                // We need to check $delta to make sure we are saving the right block.
                if ($delta == 'stats') {
                        // Have Drupal save the string to the database.
                        variable_set('mediastats_api_url', $edit['mediastats_api_url']);
                        variable_set('mediastats_pools', $edit['mediastats_pools']);
                        variable_set('mediastats_cache_results', $edit['mediastats_cache_results']);
                        variable_set('mediastats_expire_cache_seconds', $edit['mediastats_expire_cache_seconds']);
                }
        }

        /**
         * Implements hook_block_view().
         *
         * This hook generates the contents of the blocks themselves.
         */
        function mediastats_block_view($delta = '') {
                // The $delta parameter tells us which block is being requested.
                switch ($delta) {
                        case 'stats':
                                $data = media_stats_get_data();

                                if ($data == NULL) {
                                        $block['content'] = "Data Unavailable";
                                }

                                else {
                                        $block['content'] = display_table($data);

                                        $server_time = $data->{'server_time'};
                                        $dt = new DateTime("@$server_time");
                                        $dt->setTimeZone(new DateTimeZone(drupal_get_user_timezone()));
                                        $block['content'] .= "Data retrieved at: " . $dt->format('Y-m-d H:i:s');
                                }

                                break;
                }

                return $block;
        }
